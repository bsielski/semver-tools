#!/usr/bin/env python3
import sys

import semantic_version


def die(*lines):
    for line in lines:
        print(line)
    sys.exit(1)


def verify(tag):
    if tag.startswith('v'):
        tag = tag[1:]

    try:
        version = semantic_version.Version(tag)
    except ValueError as err:
        die(
            'Error: git tags should be a valid semantic version',
            '    For information about semantic versions see: https://semver.org',
        )

    if version.build:
        die('Error: git tags should not contain build metadata')

    if version.prerelease:
        try:
            int(version.prerelease[-1])
        except ValueError as err:
            die(
                'Error: git tags should not end in a non-numeric identifier',
                '    If the tag ends in something like "-rc1" it is recommended to separate the',
                '    non-numeric part from the numeric part with a dot (like this "-rc.1")',
            )


def _main():
    stdin = sys.stdin
    verify(next(stdin))


if __name__ == '__main__':
    _main()
